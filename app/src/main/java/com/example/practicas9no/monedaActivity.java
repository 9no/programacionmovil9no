package com.example.practicas9no;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import java.util.HashMap;

public class monedaActivity extends AppCompatActivity {

    private EditText txtPesos;
    private Spinner spMonedas;
    private TextView txtResultado;
    private Button btnCalcular, btnLimpiar, btnCerrar;

    private HashMap<String, Double> conversiones;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_moneda);

        setContentView(R.layout.activity_moneda);

        iniciarComponentes();

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pesosStr = txtPesos.getText().toString();
                if (pesosStr.isEmpty()) {
                    Toast.makeText(monedaActivity.this, "Por favor, ingrese una cantidad a convertir", Toast.LENGTH_SHORT).show();
                    return;
                }

                double pesos = Double.parseDouble(pesosStr);
                String monedaSeleccionada = spMonedas.getSelectedItem().toString();
                double tasaConversion = conversiones.get(monedaSeleccionada);
                double resultado = pesos * tasaConversion;

                txtResultado.setText("Total: " + String.format("%.2f", resultado) + " " + monedaSeleccionada);
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtPesos.setText("");
                txtResultado.setText("Total:");
                spMonedas.setSelection(0);
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishAffinity();
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    public void iniciarComponentes() {
        txtPesos = findViewById(R.id.txtPesos);
        spMonedas = findViewById(R.id.spMonedas);
        txtResultado = findViewById(R.id.txtResultado);


        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnCerrar = findViewById(R.id.btnCerrar);

        // Se definen los factores de conversión
        conversiones = new HashMap<>();
        conversiones.put("USD", 0.059933);  // Ejemplo: 1 Peso = 0.05 USD
        conversiones.put("EUR", 0.055247);
        conversiones.put("GBP", 0.047034);
        conversiones.put("CAD", 0.081889);

        // Crear una lista de monedas
        String[] monedas = {"USD", "EUR", "GBP", "CAD"};

        // Configurar el Spinner con el ArrayAdapter
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, monedas);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spMonedas.setAdapter(adapter);
    }

}